/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ipd23.day02twodimarrays;

import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;

/**
 *
 * @author Artem
 */
public class Day02TwoDimArrays {

    static Scanner input = new Scanner(System.in);
    static int[][] data;

    /**
     * @param args the command line arguments
     */
   
    static void stdDev(int[][] data){
        int sum = 0;
        for (int row = 0; row < data.length; row++) {
            for (int col = 0; col < data[row].length; col++) {
                sum += data[row][col];
            }
        }
        double avg = (double)sum / (data.length * data[0].length);
        double sumOfSquares = 0;
        for (int row = 0; row < data.length; row++) {
            for (int col = 0; col < data[row].length; col++) {
                int val = data[row][col];
                sumOfSquares += (val - avg)*(val - avg);
            }
        }
        double variance = sumOfSquares / (data.length * data[0].length);
        double stdDev = Math.sqrt(variance);
        System.out.printf("Standard deviation is: %.3f\n", stdDev);
        System.out.println("");
    }
    static void printArray(int[][] data){
        for (int row = 0; row < data.length; row++) {
            for (int col = 0; col < data[row].length; col++) {
                System.out.printf("%s%d", col==0 ? "" : ", ", data[row][col]);
            }
            System.out.print("\n");
        }
        System.out.println("");
    }
    static void sumOfCols(int[][] data){
        for (int col = 0; col < data[0].length; col++) {
            int sum = 0;
            for (int row = 0; row < data.length; row++) {
                sum += data[row][col];
            }
            System.out.printf("Sum of col %d is: %d\n", col, sum);
        }
        System.out.println("");
    }
    static int[][] getTwoDimentionalArray(){
    System.out.println("Enter the width of the array (1 or greater): ");
        int width = getInt();
        System.out.println("Enter the height of the array (1 or greater): ");
        int height = getInt();
        data = new int[width][height];
        for (int row = 0; row < data.length; row++) {
            for (int col = 0; col < data[row].length; col++) {
                data[row][col] = (int) (Math.random() * 201) - 100;
            }
        }
        return data;
    }
    static void sumOfrows(int[][] data){
    int sum = 0;
        for (int row = 0; row < data.length; row++) {
            sum = 0;
            for (int col = 0; col < data[row].length; col++) {
                sum += data[row][col];
            }
            System.out.printf("Sum of row %d is: %d\n", row, sum);
        }
        System.out.println("");
    }
    static void sumOfAll(int[][] data){
        int sum = 0;
        for (int row = 0; row < data.length; row++) {
            for (int col = 0; col < data[row].length; col++) {
                sum += data[row][col];
            }
        }
        System.out.println("Sum of all is: " + sum);
        System.out.println("");
    }
   static boolean isPrime(int number) {
        int sqrt = (int) Math.sqrt(number) + 1;
        for (int i = 2; i < sqrt; i++) {
            if (number % i == 0) { 
                return false;
            }
        }
        return true;
    }
   
    static void primesOfPairs(int[][] data){
        int num1 = 0;
        for (int row = 0; row < data.length; row++) {
            for (int col = 0; col < data[row].length; col++) {
                num1 = data[row][col];
                
                for (int row2 = row; row2 < data.length; row2++) {
                    for (int col2 = 0; col2 < data[row2].length; col2++) {
                        if(row2==row && col2 <= col){
                            continue;
                        }
                        int num2 = data[row2][col2];
                        int sum = num1+num2;
                        if (sum > 0 && isPrime(sum))
                        {
                            System.out.printf("Sum of %d + %d = %d is prime\n", num1, num2, num1+num2);
                        }
                        
                    }
                }
            }
        }
        System.out.println("");
    }
   
    static int getInt() {
        Scanner input = new Scanner(System.in);
        int value = -1;
        while (value < 1) {
            try {
                if (input.hasNextInt()) {
                    value = input.nextInt();
                    if (value <= 0) {
                        throw new InputMismatchException("The number is negative or zero: " + value);
                    }
                } // end if
                else {
                    String junk = input.next();
                    throw new InputMismatchException("Input is non-digit number: " + junk);
                } // end else
            } // end try
            catch (InputMismatchException e) {
                System.out.println(e.getMessage());
                System.out.println("Enter the positive number: ");
            } // catch
        } // end while
        return value;
    } //end method getInt
    
     public static void main(String[] args) {
        var data = getTwoDimentionalArray();
        printArray(data);
        sumOfAll(data);
        sumOfrows(data);
        sumOfCols(data);
        stdDev(data);
        primesOfPairs(data);
    }
}

