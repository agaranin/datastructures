/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ipd23.day03linkedlistarray;

/**
 *
 * @author Artem
 */
public class Day03LinkedListArray {
static <T> void printArray(T[] data) {
        for (int i = 0; i < data.length; i++) {
            System.out.printf("%s%s", i == 0 ? "" : ", ", (String)data[i]);
        }
    }
    public static void main(String[] args) {
        LinkedListArray<String> a = new LinkedListArray<String>();
        a.add("aaa");
        a.add("bbb");
        a.add("ccc");
        a.add("ddd");
        System.out.println(a.toString());
        a.insertValueAtIndex(4, "eee");
        System.out.println(a.toString());
        a.insertValueAtIndex(2, "zzz");
        System.out.println(a.toString());
        a.deleteByIndex(2);
        System.out.println(a.toString());
        a.deleteByIndex(4);
        a.deleteByIndex(0);
        System.out.println(a.toString());
        LinkedListArrayOfStrings a2 = new LinkedListArrayOfStrings();
        a2.add("aaa1");
        a2.insertValueAtIndex(0, "aaa2");
        a2.add("aaa3");
        System.out.println(a2.toString());
        if (a2.deleteByValue("aaa3")) {
            System.out.println("deleted");
        } else {
            System.out.println("Not found");
        }
        System.out.println(a.toString());
        a.toArray();
    }
}
