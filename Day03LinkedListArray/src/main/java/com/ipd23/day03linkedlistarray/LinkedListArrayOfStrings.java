/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ipd23.day03linkedlistarray;

/**
 *
 * @author Artem
 */
public class LinkedListArrayOfStrings {

    private class Container {

        private Container(Container next, String value) {
            this.next = next;
            this.value = value;
        }
        Container next;
        String value;
    }
    private Container start;
    private Container end;
    private int size = 0;

    public void add(String value) {
        if (size == 0) {
            start = new Container(null, null);
            end = start;
        }
        end.next = new Container(null, value);
        end = end.next;
        size++;
    }

    public String get(int index) {
        if (index >= size || index < 0) {
            throw new IndexOutOfBoundsException();
        }
        int count = 0;
        Container found = start.next;
        while (count < index) {
            found = found.next;
            count++;
        }
        return found.value;
    }

    public void insertValueAtIndex(int index, String value) {
        if (index > size || index < 0) {
            throw new IndexOutOfBoundsException();
        }
        if (index == size || size == 0) {
            add(value);
            return;
        }
        int count = -1;
        Container before = start;
        while (count++ < index - 1) {
            before = before.next;
        }
        before.next = new Container(before.next, value);
        size++;
    }

    public void deleteByIndex(int index) {
        if (index == 0) {
            start = start.next;
            size--;
            return;
        }
        int count = -1;
        Container before = start;
        while (count < index - 1) {
            before = before.next;
            count++;
        }
        if (index == size - 1) {
            before.next = null;
            end = before;
            size--;
            return;
        }
        before.next = before.next.next;
        size--;
    }

    public boolean deleteByValue(String value) {
        int count = -1;
        Container found = start;
        Container prev = start;
        while (!value.equals(found.value)) {
            count++;
            if (count > size - 1) {
                return false;
            }
            prev = found;
            found = found.next;
        }
        if (count == size - 1) {
            found.next = null;
            end = found;
            size--;
            return true;
        } else if (count == 0) {
            start = start.next;
            size--;
            return true;
        } else {
            prev.next = found.next;
            size--;
            return true;
        }
    } // delete first value found

    public int getSize() {
        return size;
    }

    @Override
    public String toString() {
        int count = -1;
        Container found = start;
        StringBuilder sb = new StringBuilder();
        sb.append("[");
        while (true) {
            count++;
            if (count > size - 1) {
                break;
            }
            sb.append(count == 0 ? "" : ", ");
            sb.append(found.next.value);
            found = found.next;
        }
        sb.append("]");
        return sb.toString();
    } // returns String similar to: [3, 5, 6, -23]

    public String[] toArray() {
        var result = new String[size];
        int count = -1;
        Container found = start;
        StringBuilder sb = new StringBuilder();
        while (true) {
            count++;
            if (count > size - 1) {
                break;
            }
            result[count] = found.next.value;
            found = found.next;
        }
        return result;
    } // could be used for Unit testing
}
