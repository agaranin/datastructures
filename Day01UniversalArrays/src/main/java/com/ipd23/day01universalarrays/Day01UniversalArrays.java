/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ipd23.day01universalarrays;

import java.util.Arrays;
import java.util.InputMismatchException;
import java.util.Scanner;

/**
 *
 * @author Artem
 */
public class Day01UniversalArrays {

    static int getInt() {
        Scanner input = new Scanner(System.in);
        int value = -1;
        while (true) {
            try {
                if (input.hasNextInt()) {
                    value = input.nextInt();
                    return value;
                } // end if
                else {
                    String junk = input.next();
                    throw new InputMismatchException("Input is not an integer: " + junk);
                } // end else
            } // end try
            catch (InputMismatchException e) {
                System.out.println(e.getMessage());
                System.out.println("Enter an integer: ");
            } // catch
        } // end while
    } //end method getInt

    static void inputArray(int[] data) {
        for (int i = 0; i < data.length; i++) {
            System.out.printf("Enter value %d: \n", i + 1);
            data[i] = getInt();
        }
    }

    static void inputArray(int[][] data2d) {
        for (int row = 0; row < data2d.length; row++) {
            for (int col = 0; col < data2d[row].length; col++) {
                System.out.printf("Enter value row %d column %d: \n", row + 1, col + 1);
                data2d[row][col] = getInt();
            }
        }
    }

    static void printArray(int[] data) {
        for (int i = 0; i < data.length; i++) {
            System.out.printf("%s%d", i == 0 ? "" : ", ", data[i]);
        }
    }

    static void printArray(int[][] data2d) {
        for (int row = 0; row < data2d.length; row++) {
            for (int col = 0; col < data2d[row].length; col++) {
                int depth = 5;
                System.out.printf("%s%d", col == 0 ? "" : ", ", data2d[row][col]);
            }
            System.out.println("");
        }
        System.out.println("");
    }

    static void printArrayAdv(int[][] data2d) {
        // find the maximum row length
        int maxRowLength = data2d[0].length;
        for (int row = 0; row < data2d.length; row++) {
            if (data2d[row].length > maxRowLength) {
                maxRowLength = data2d[row].length;
            }
        }
        // get columns width
        var columnsWidth = new int[maxRowLength];
        for (int col = 0; col < maxRowLength; col++) {
            columnsWidth[col] = 0;
            for (int row = 0; row < data2d.length; row++) {
                if (col >= data2d[row].length) {
                    continue;
                }
                int length = String.valueOf(data2d[row][col]).length();
                if (columnsWidth[col] < length) {
                    columnsWidth[col] = String.valueOf(data2d[row][col]).length();
                }
            }
        }
        //printArray(columnsWidth);
        // Print Array
        System.out.println("");
        for (int row = 0; row < data2d.length; row++) {
            for (int col = 0; col < data2d[row].length; col++) {
                System.out.printf("%s%" + columnsWidth[col] + "d", col == 0 ? "" : ", ", data2d[row][col]);
            }
            System.out.println("");
        }
    }

    static int[] findDuplicates(int[] a1, int[] a2) {
        boolean isDuplicate = false;
        int numOfDupl = 0;
        for (int i = 0; i < a1.length; i++) {
            for (int j = 0; j < a2.length; j++) {
                if (a1[i] == a2[j]) {
                    isDuplicate = true;
                }
            }
            if (isDuplicate) {
                numOfDupl++;
                isDuplicate = false;
            }
        }

        var dupl = new int[numOfDupl];
        int n = 0;
        for (int i = 0; i < a1.length; i++) {
            for (int j = 0; j < a2.length; j++) {
                if (a1[i] == a2[j]) {
                    isDuplicate = true;
                    dupl[n] = a2[j];

                }
            }
            if (isDuplicate) {
                if (n < dupl.length - 1) {
                    n++;
                    isDuplicate = false;
                }
            }
        }
        return removeDuplicates(dupl);
    }

    static int[] removeDuplicates(int[] a1) {
        int duplCount = 0;
        for (int i = 0; i < a1.length; i++) {
            for (int j = 0; j < a1.length - duplCount; j++) {
                if (j <= i) {
                    continue;
                }
                if (a1[i] == a1[j]) {
                    a1[j] = a1[a1.length - 1 - duplCount];
                    duplCount++;
                }
            }
        }

        //var a2 = Arrays.copyOf(a1, a1.length - duplCount);
        var a2 = new int[a1.length - duplCount];
        for (int i = 0; i < a2.length; i++) {
            a2[i] = a1[i];
        }
        return a2;
    }

    static int[] findDuplicates(int[][] a1, int[][] a2) {
        // find the number of duplicates
        boolean isDuplicate = false;
        int numOfDupl = 0;
        for (int i = 0; i < a1.length; i++) {
            for (int j = 0; j < a1[i].length; j++) {
                for (int k = 0; k < a2.length; k++) {
                    for (int l = 0; l < a2[k].length; l++) {
                        if (a1[i][j] == a2[k][l]) {
                            isDuplicate = true;
                        }
                    }
                }
                if (isDuplicate) {
                    numOfDupl++;
                    isDuplicate = false;
                }
            }

        }

        var dupl = new int[numOfDupl];
        int n = 0;
        for (int i = 0; i < a1.length; i++) {
            for (int j = 0; j < a1[i].length; j++) {
                for (int k = 0; k < a2.length; k++) {
                    for (int l = 0; l < a2[k].length; l++) {
                        if (a1[i][j] == a2[k][l]) {
                            isDuplicate = true;
                            dupl[n] = a2[k][l];
                        }
                    }
                }
                if (isDuplicate) {
                    if (n < dupl.length - 1) {
                        n++;
                        isDuplicate = false;
                    }
                }
            }

        }
        return removeDuplicates(dupl);
    }

    static int[] join(int[] a1, int[] a2) {
        var joinArr = new int[a1.length + a2.length];
        for (int i = 0; i < a1.length; i++) {
            joinArr[i] = a1[i];
        }
        int k = a1.length;
        for (int j = 0; j < a2.length; j++) {
            joinArr[k] = a2[j];
            k++;
        }
        return joinArr;
    }

    public static void main(String[] args) {

        //Part 1
        var data = new int[5];
        inputArray(data);
        /*
        //Part 2
        var data2d = new int[2][3];
        inputArray(data2d);
        
          //Part 3
          var data = new int[5];
          inputArray(data);
          printArray(data);
          
        //Part 4
        int[][] data2d = {
            new int[]{10234234, 20, -930, 4},
            new int[]{-930, 4},
            new int[]{506, 60, 70, 80, 90, 100},
            new int[]{6, 70, 80, 90, 100}
        };
        printArray(data2d);
        
        //Part 4 Advanced
        printArrayAdv(data2d);
         //Part 5 Find Duplicates
        int[] a1 = {1, 2, 3, 2, 3, 5};
        int[] a2 = {7, 5, 3, 3, 0};
        var dupl = findDuplicates(a1, a2);
        printArray(dupl);
        
        //Part 6
        int[][] a1 = {
            new int[]{10, 20, -930, 4},
            new int[]{-930, 4},
            new int[]{506, 60, 70, 80, 90, 100},
            new int[]{6, 70, 80, 90, 100}
        };
        int[][] a2 = {
            new int[]{34, 3, 70, 34},
            new int[]{20, 4},
            new int[]{546, 650, 77, 80, 90, 102},
            new int[]{56, 70, 83, 4, 120}
        };
        var result =  findDuplicates(a1,a2);
        printArray(result);
        
        //Part 7 Find Duplicates
        int[] a1 = {1, 2, 3, 2, 3, 5};
        int[] a2 = {7, 5, 3, 3, 0};
        var joinArr = join(a1, a2);
        printArray(joinArr);
         */
    }
}
