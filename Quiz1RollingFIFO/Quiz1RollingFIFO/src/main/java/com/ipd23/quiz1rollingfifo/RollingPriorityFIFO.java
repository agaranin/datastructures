/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ipd23.quiz1rollingfifo;

class FIFOFullException extends Exception {
}

/**
 *
 * @author Artem
 */
public class RollingPriorityFIFO {

    private class Item {

        // add constructor of your choice
        public Item(Item next, boolean priority, String value) {
            this.next = next;
            this.priority = priority;
            this.value = value;
        }

        Item next;
        boolean priority;
        String value;
    }
    private Item tail, head; // enqueue at tail, dequeue at head
    int itemsTotal, itemsCurrUsed;

    public RollingPriorityFIFO(int itemsTotal) {
        this.itemsTotal = itemsTotal;
        for (int i = 0; i < itemsTotal; i++) {
            Item newItem = new Item(head, false, null);
            if (i == 0) {
                head = newItem;
                tail = newItem;
            } else {
                tail.next = newItem;
                tail = newItem;
            }
        }
    }

    public void enqueue(String value, boolean priority) throws FIFOFullException {
        if (itemsCurrUsed == itemsTotal) {
            throw new FIFOFullException();
        }
        tail.next.value = value;
        tail.next.priority = priority;
        tail = tail.next;
        itemsCurrUsed++;
    }

    public String dequeue() {
        String result;
        if (itemsCurrUsed == 0) {
            return null;
        }
        Item previous = tail;
        Item current = head;
        for (int i = 0; i < itemsTotal; i++) {
            if (current.priority) {
                if (current == tail) {
                    result = current.value;
                    current.value = null;
                    current.priority = false;
                    tail = previous;
                    itemsCurrUsed--;
                    return result;
                }
                result = current.value;
                current.value = null;
                current.priority = false;
                previous.next = current.next;
                current.next = tail.next;
                tail.next = current;
                itemsCurrUsed--;
                return result;
            }
            previous = current;
            current = current.next;
        }
        result = head.value;
        head.value = null;
        head.priority = false;
        head = head.next;
        itemsCurrUsed--;
        return result;
    }

    public int size() {
        return itemsCurrUsed;
    } // current FIFO size

    public int sizeMax() {
        return itemsTotal;
    } // maximum FIFO size

    // Returns array of Strings of all items in FIFO (following next references).
    public String[] toArray() {
        Item current = head;
        var result = new String[itemsCurrUsed];
        int j = 0;
        for (int i = 0; i < itemsTotal; i++) {
            if (current.value != null) {
                result[j] = current.value;
                j++;
            }
            current = current.next;
        }
        return result;
    }
    // Returns array of String only of priority items in FIFO.

    public String[] toArrayOnlyPriority() {
        Item current = head;
        var temp = new String[itemsCurrUsed];
        int j = 0;
        for (int i = 0; i < itemsTotal; i++) {
            if (current.value != null) {
                if (current.priority) {
                    temp[j] = current.value;
                    j++;
                }
            }
            current = current.next;
        }
        var result = new String[j];
        for (int i = 0; i < j; i++) {
            result[i] = temp[i];
        }
        return result;
    }
// Items with priority=true have "*" appended, e.g. "[Jerry*,Maria,Tom*];
    // Items with priority=true have "*" appended, e.g. "[Jerry*,Maria,Tom*];
    // Items about to be dequeue()'d are listed first, items recently enqueue()'d last

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("[");
        Item current = head;
        for (int i = 0; i < itemsTotal; i++) {
            if (current.value != null) {
                sb.append(i == 0 ? "" : ",");
                sb.append(current.value);
                if (current.priority) {
                    sb.append("*");
                }
            }
            current = current.next;
        }
        sb.append("]");
        return sb.toString();
    }
}
