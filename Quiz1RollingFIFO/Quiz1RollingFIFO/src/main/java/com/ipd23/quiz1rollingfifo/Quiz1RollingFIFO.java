/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ipd23.quiz1rollingfifo;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Artem
 */
public class Quiz1RollingFIFO {

     static void printArray(String[] data) {
        for (int i = 0; i < data.length; i++) {
            System.out.printf("%s%s", i == 0 ? "" : ", ", data[i]);
        }
    }
    public static void main(String[] args) {
        RollingPriorityFIFO fifo = new RollingPriorityFIFO(5);
        System.out.println(fifo.sizeMax());
        try {
            fifo.enqueue("value1", false);
            fifo.enqueue("value2", false);
            fifo.enqueue("value3", true);
            fifo.enqueue("value4", true);
            fifo.enqueue("value5", true);
            fifo.enqueue("value6", false);
        } catch (FIFOFullException ex) {
            System.out.println("Fifo is full");
        }
        System.out.println(fifo.toString());
        System.out.println("size: " + fifo.size());
        printArray(fifo.toArray());
        System.out.println("");
        printArray(fifo.toArrayOnlyPriority());
         System.out.println("");
        System.out.println("Dequeue: " + fifo.dequeue());
        System.out.println("size: " + fifo.size());
        System.out.println(fifo.toString());
    }
    
}
