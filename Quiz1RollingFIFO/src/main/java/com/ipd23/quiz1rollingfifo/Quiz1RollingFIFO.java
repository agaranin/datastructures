/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ipd23.quiz1rollingfifo;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Artem
 */
public class Quiz1RollingFIFO {

    static void printArray(String[] data) {
        for (int i = 0; i < data.length; i++) {
            System.out.printf("%s%s", i == 0 ? "" : ", ", data[i]);
        }
    }

    public static void main(String[] args) throws FIFOFullException {
//        RollingPriorityFIFO fifo = new RollingPriorityFIFO(5);
//        System.out.println(fifo.sizeMax());
//        try {
//            fifo.enqueue("value1", false);
//            fifo.enqueue("value2", false);
//            fifo.enqueue("value3", true);
//            fifo.enqueue("value4", true);
//            fifo.enqueue("value5", true);
//            fifo.enqueue("value6", false);
//        } catch (FIFOFullException ex) {
//            System.out.println("Fifo is full");
//        }
//        System.out.println(fifo.toString());
//        System.out.println("size: " + fifo.size());
//        printArray(fifo.toArray());
//        System.out.println("");
//        printArray(fifo.toArrayOnlyPriority());
//         System.out.println("");
//        System.out.println("Dequeue: " + fifo.dequeue());
//        System.out.println("size: " + fifo.size());
//        System.out.println(fifo.toString());
//RollingPriorityFIFO fifo = new RollingPriorityFIFO(5);
//        fifo.enqueue("Albert", true); // prio
//        fifo.enqueue("Barry", false);
//        fifo.enqueue("Charlie", true); // prio
//        fifo.enqueue("Darryl", false);
//        fifo.enqueue("Emily", true); // prio
//        for (String expected : new String[]{"Albert", "Charlie", "Emily", "Barry", "Darryl"}) {
//            String result = fifo.dequeue();
//            System.out.println(expected +" -> "+result);
//        }
//        String resultNull = fifo.dequeue(); // queue should be empty
//        System.out.println(null +" -> "+resultNull);

        RollingPriorityFIFO fifo = new RollingPriorityFIFO(5);
        fifo.enqueue("Albert", true); // priority - first item
        fifo.enqueue("Barry", false);
        fifo.enqueue("Charlie", false);
        System.out.println(fifo.toString());
        String result1 = fifo.dequeue(); // priority dequeue from the middle
        System.out.println("Albert" + "->" + result1);
        System.out.println(fifo.toString());
        // todo: add more operations afterwards
        fifo.enqueue("Eva", false);
        fifo.enqueue("Marry", false);
        String result2 = fifo.dequeue();
        System.out.println("Barry" + "->" + result2);
        for (String expected : new String[]{"Charlie", "Eva", "Marry"}) {
            String result = fifo.dequeue();
            System.out.println(expected + "->" + result);
        }
        String resultNull = fifo.dequeue(); // queue should be empty
        System.out.println(null + " -> " + resultNull);
    }
}
