/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ipd23.quiz1rollingfifo;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Artem
 */
public class RollingPriorityFIFOTest {

    public RollingPriorityFIFOTest() {
    }

    /**
     * Test of enqueue method, of class RollingPriorityFIFO.
     */
    @Test
    public void testEnqueue() throws Exception {
        RollingPriorityFIFO instance = new RollingPriorityFIFO(5);
        instance.enqueue("value1", false);
        instance.enqueue("value2", false);
        instance.enqueue("value3", true);
        instance.enqueue("value4", true);
        instance.enqueue("value5", true);
        assertEquals(5, instance.size());
        assertEquals("[value1,value2,value3*,value4*,value5*]", instance.toString());
    }

    /**
     * Test of enqueue method, of class RollingPriorityFIFO.
     */
    @Test(expected = FIFOFullException.class)
    public void testEnqueueThrowsException() throws Exception {
        RollingPriorityFIFO instance = new RollingPriorityFIFO(5);
        instance.enqueue("value1", false);
        instance.enqueue("value2", false);
        instance.enqueue("value3", true);
        instance.enqueue("value4", true);
        instance.enqueue("value5", true);
        instance.enqueue("value6", true);
    }

    /**
     * Test of dequeue method, of class RollingPriorityFIFO.
     */
    @Test
    public void testDequeueReturnsNonPriorityItem() throws Exception {
        RollingPriorityFIFO instance = new RollingPriorityFIFO(5);
        instance.enqueue("value1", false);
        instance.enqueue("value2", false);
        instance.enqueue("value3", false);
        instance.enqueue("value4", false);
        instance.enqueue("value5", false);
        String expected = "value1";
        String actual = instance.dequeue();
        assertEquals(expected, actual);
        assertEquals(4, instance.size());
        assertEquals("[value2,value3,value4,value5]", instance.toString());
    }

    /**
     * Test of dequeue method, of class RollingPriorityFIFO.
     */
    @Test
    public void testDequeueReturnsPriorityItem() throws Exception {
        RollingPriorityFIFO instance = new RollingPriorityFIFO(5);
        instance.enqueue("value1", false);
        instance.enqueue("value2", false);
        instance.enqueue("value3", true);
        instance.enqueue("value4", false);
        instance.enqueue("value5", false);
        String expected = "value3";
        String actual = instance.dequeue();
        assertEquals(expected, actual);
        assertEquals(4, instance.size());
        assertEquals("[value1,value2,value4,value5]", instance.toString());
    }

    /**
     * Test of dequeue method, of class RollingPriorityFIFO.
     */
    @Test
    public void testDequeueReturnsPriorityItemBeingTail() throws Exception {
        RollingPriorityFIFO instance = new RollingPriorityFIFO(5);
        instance.enqueue("value1", false);
        instance.enqueue("value2", false);
        instance.enqueue("value3", false);
        instance.enqueue("value4", false);
        instance.enqueue("value5", true);
        String expected = "value5";
        String actual = instance.dequeue();
        assertEquals(expected, actual);
        assertEquals(4, instance.size());
        assertEquals("[value1,value2,value3,value4]", instance.toString());
    }

    /**
     * Test of dequeue method, of class RollingPriorityFIFO.
     */
    @Test
    public void testDequeueReturnsNull() throws Exception {
        RollingPriorityFIFO instance = new RollingPriorityFIFO(5);
        String expected = null;
        String actual = instance.dequeue();
        assertEquals(expected, actual);
        assertEquals(0, instance.size());
        assertEquals("[]", instance.toString());
    }

    /**
     * Test of size method, of class RollingPriorityFIFO.
     */
    @Test
    public void testSize() throws Exception {
        RollingPriorityFIFO instance = new RollingPriorityFIFO(5);
        assertEquals(0, instance.size());
        instance.enqueue("value1", false);
        instance.enqueue("value2", false);
        assertEquals(2, instance.size());
        instance.enqueue("value3", true);
        instance.enqueue("value4", false);
        instance.enqueue("value5", false);
        assertEquals(5, instance.size());
        instance.dequeue();
        assertEquals(4, instance.size());
        instance.dequeue();
        instance.dequeue();
        instance.dequeue();
        instance.dequeue();
        assertEquals(0, instance.size());

    }

    /**
     * Test of sizeMax method, of class RollingPriorityFIFO.
     */
    @Test
    public void testSizeMax() {
        RollingPriorityFIFO instance = new RollingPriorityFIFO(5);
        assertEquals(5, instance.sizeMax());
    }

    /**
     * Test of toArray method, of class RollingPriorityFIFO.
     */
    @Test
    public void testToArray() throws Exception {
        RollingPriorityFIFO instance = new RollingPriorityFIFO(5);
        instance.enqueue("value1", false);
        instance.enqueue("value2", false);
        instance.enqueue("value3", false);
        instance.enqueue("value4", false);
        instance.enqueue("value5", false);
        String[] expected = {"value1", "value2", "value3", "value4", "value5"};
        String[] actual = instance.toArray();
        assertArrayEquals(expected, actual);
    }

    /**
     * Test of toArrayOnlyPriority method, of class RollingPriorityFIFO.
     */
    @Test
    public void testToArrayOnlyPriority() throws Exception{
        RollingPriorityFIFO instance = new RollingPriorityFIFO(5);
        instance.enqueue("value1", true);
        instance.enqueue("value2", true);
        instance.enqueue("value3", false);
        instance.enqueue("value4", true);
        instance.enqueue("value5", false);
        String[] expected = {"value1", "value2", "value4"};
        String[] actual = instance.toArrayOnlyPriority();
        assertArrayEquals(expected, actual);
    }

    /**
     * Test of toString method, of class RollingPriorityFIFO.
     */
    @Test
    public void testToString() throws Exception{
         RollingPriorityFIFO instance = new RollingPriorityFIFO(5);
        assertEquals("[]", instance.toString());
        instance.enqueue("value1", true);
        instance.enqueue("value2", true);
        instance.enqueue("value3", false);
        assertEquals("[value1*,value2*,value3]", instance.toString());
        instance.enqueue("value4", true);
        instance.enqueue("value5", false);
        assertEquals("[value1*,value2*,value3,value4*,value5]", instance.toString());
    }
//================================================================
    @Test(timeout=1000)
    public void InOutOne() throws FIFOFullException {
        RollingPriorityFIFO fifo = new RollingPriorityFIFO(5);
        fifo.enqueue("Jerry", false);
        String result = fifo.dequeue();
        assertEquals("Jerry", result);
    }
    
    @Test(timeout=1000)
    public void InOutTwo() throws FIFOFullException {
        RollingPriorityFIFO fifo = new RollingPriorityFIFO(5);
        fifo.enqueue("Jerry", false);
        fifo.enqueue("Terry", false);
        String result1 = fifo.dequeue();
        assertEquals("Jerry", result1);
        String result2 = fifo.dequeue();
        assertEquals("Terry", result2);
    }
    
    @Test(timeout=1000)
    public void InOutThreeMix() throws FIFOFullException {
        RollingPriorityFIFO fifo = new RollingPriorityFIFO(5);
        fifo.enqueue("Jerry", false);
        fifo.enqueue("Terry", false);
        fifo.enqueue("Eva", false);
        String result1 = fifo.dequeue();
        assertEquals("Jerry", result1);
        fifo.enqueue("Marry", false);
        for (String expected : new String[]{"Terry", "Eva", "Marry"}) {
            String result = fifo.dequeue();
            assertEquals(expected, result);
        }
    }

    @Test(timeout=1000)
    public void InOutThreeMixToFail() throws FIFOFullException {
        RollingPriorityFIFO fifo = new RollingPriorityFIFO(5);
        fifo.enqueue("Jerry", false);
        fifo.enqueue("Terry", false);
        fifo.enqueue("Eva", false);
        String result1 = fifo.dequeue();
        assertEquals("Jerry", result1);
        fifo.enqueue("Marry", false);
        for (String expected : new String[]{"Terry", "Eva", "Marry"}) {
            String result = fifo.dequeue();
            assertEquals(expected, result);
        }
        String resultNull = fifo.dequeue(); // queue should be empty
        assertEquals(null, resultNull);
    }

    @Test(timeout=1000)
    public void SeveralItemsToArray() throws FIFOFullException {
        RollingPriorityFIFO fifo = new RollingPriorityFIFO(5);
        fifo.enqueue("Jerry", false);
        fifo.enqueue("Terry", false);
        fifo.enqueue("Eva", false);
        String []result = fifo.toArray();
        assertArrayEquals(new String[]{"Jerry", "Terry", "Eva"}, result);
    }

    @Test(timeout=1000)
    public void SeveralItemsToString() throws FIFOFullException {
        RollingPriorityFIFO fifo = new RollingPriorityFIFO(5);
        fifo.enqueue("Jerry", false);
        fifo.enqueue("Terry", true); // priority
        fifo.enqueue("Eva", false);
        String result = fifo.toString();
        assertEquals("[Jerry,Terry*,Eva]", result);
    }

    @Test(timeout=1000)
    public void SeveralItemsToArrayPriorityOnly() throws FIFOFullException {
        RollingPriorityFIFO fifo = new RollingPriorityFIFO(5);
        fifo.enqueue("Jerry", false);
        fifo.enqueue("Terry", true); // priority
        fifo.enqueue("Eva", false);
        fifo.enqueue("Evelyn", true); // priority
        String []result = fifo.toArrayOnlyPriority();
        assertArrayEquals(new String[]{"Terry", "Evelyn"}, result);
    }

    
    @Test(timeout=1000)
    public void PriorityDequeMiddle() throws FIFOFullException {
        RollingPriorityFIFO fifo = new RollingPriorityFIFO(5);
        fifo.enqueue("Albert", false);
        fifo.enqueue("Barry", true); // priority
        fifo.enqueue("Charlie", false);
        String result1 = fifo.dequeue(); // priority dequeue from the middle
        assertEquals("Barry", result1);
        // todo: add more operations afterwards
        fifo.enqueue("Eva", false);
        fifo.enqueue("Marry", false);
        String result2 = fifo.dequeue();
        assertEquals("Albert", result2);
        for (String expected : new String[]{"Charlie", "Eva", "Marry"}) {
            String result = fifo.dequeue();
            assertEquals(expected, result);
        }
        String resultNull = fifo.dequeue(); // queue should be empty
        assertEquals(null, resultNull);
    }

    @Test(timeout=1000)
    public void PriorityDequeAtEnd() throws FIFOFullException {
        RollingPriorityFIFO fifo = new RollingPriorityFIFO(5);
        fifo.enqueue("Albert", true); // priority - first item
        fifo.enqueue("Barry", false);
        fifo.enqueue("Charlie", false);
        String result1 = fifo.dequeue(); // priority dequeue from the middle
        assertEquals("Albert", result1);
        // todo: add more operations afterwards
        fifo.enqueue("Eva", false);
        fifo.enqueue("Marry", false);
        String result2 = fifo.dequeue();
        assertEquals("Barry", result2);
        for (String expected : new String[]{"Charlie", "Eva", "Marry"}) {
            String result = fifo.dequeue();
            assertEquals(expected, result);
        }
        String resultNull = fifo.dequeue(); // queue should be empty
        assertEquals(null, resultNull);
    }

    @Test(timeout=1000)
    public void PriorityDequeAtStart() throws FIFOFullException {
        RollingPriorityFIFO fifo = new RollingPriorityFIFO(5);
        fifo.enqueue("Albert", false);
        fifo.enqueue("Barry", false);
        fifo.enqueue("Charlie", true); // priority - last item
        String result1 = fifo.dequeue(); // priority dequeue from the middle
        assertEquals("Charlie", result1);
        // todo: add more operations afterwards
        fifo.enqueue("Eva", false);
        fifo.enqueue("Marry", false);
        String result2 = fifo.dequeue();
        assertEquals("Albert", result2);
        for (String expected : new String[]{"Barry", "Eva", "Marry"}) {
            String result = fifo.dequeue();
            assertEquals(expected, result);
        }
        String resultNull = fifo.dequeue(); // queue should be empty
        assertEquals(null, resultNull);
    }

    @Test(timeout=1000)
    public void PriorityDequeMultiple() throws FIFOFullException {
        RollingPriorityFIFO fifo = new RollingPriorityFIFO(5);
        fifo.enqueue("Albert", true); // prio
        fifo.enqueue("Barry", false);
        fifo.enqueue("Charlie", true); // prio
        fifo.enqueue("Darryl", false);
        fifo.enqueue("Emily", true); // prio
        for (String expected : new String[]{"Albert", "Charlie", "Emily", "Barry", "Darryl"}) {
            String result = fifo.dequeue();
            assertEquals(expected, result);
        }
        String resultNull = fifo.dequeue(); // queue should be empty
        assertEquals(null, resultNull);
    }
}
