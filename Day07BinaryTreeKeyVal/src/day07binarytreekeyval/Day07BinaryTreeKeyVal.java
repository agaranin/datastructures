/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package day07binarytreekeyval;

/**
 *
 * @author Artem
 */
public class Day07BinaryTreeKeyVal {

    static void printArray(String[] data) {
        for (int i = 0; i < data.length; i++) {
            System.out.printf("%s%s", i == 0 ? "" : ", ", data[i]);
        }
    }
    

    public static void main(String[] args) {
        BinaryTreeStringInt bt = new BinaryTreeStringInt();
        bt.put("aaa", 5);
        bt.put("bbb", 3);
        bt.put("ccc", 1);
        bt.put("ddd", 4);
        bt.put("eee", 8);
        System.out.println(bt.getSumOfAllValues());
        printArray(bt.getKeysInOrder());
        System.out.println("");

        for (var n : bt) // foreach
        {
            System.out.printf("- Key %s, Value: %d\n", n.key, n.value);
        }
        bt.printAllKeyValPairs();
    }

}
