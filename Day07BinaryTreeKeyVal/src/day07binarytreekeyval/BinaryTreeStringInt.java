/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package day07binarytreekeyval;

import java.util.Iterator;

/**
 *
 * @author Artem
 */
public class BinaryTreeStringInt implements Iterable<BinaryTreeStringInt.NodeKV> {

    protected class NodeKV {

        public NodeKV() {

        }

        public NodeKV(String key, int value, NodeKV left, NodeKV right) {
            this.key = key;
            this.value = value;
            this.left = left;
            this.right = right;
        }

        String key;
        int value; // could also be key,value pair
        NodeKV left, right;
    }

    NodeKV root;
    private int nodesCount;

    // throws exception if put attempts to insert value that already exists (a duplicate)
    void put(String key, int value) throws IllegalArgumentException {
        NodeKV newNode = new NodeKV(key, value, null, null);
        if (root == null) {
            root = newNode;
            nodesCount++;
            return;
        }
        NodeKV current = root;
        while (true) {
            if (current.key.equals(key)) {
                current.value = value;
            }
            if (key.compareToIgnoreCase(current.key) < 0) {
                if (current.left == null) {
                    current.left = newNode;
                    nodesCount++;
                    return;
                } else {
                    current = current.left;
                    continue;
                }
            }
            if (key.compareToIgnoreCase(current.key) > 0) {
                if (current.right == null) {
                    current.right = newNode;
                    nodesCount++;
                    return;
                } else {
                    current = current.right;
                    continue;
                }
            }
        }

    }

    // uses recursion to compute the sum of all values in the entire tree
    public int getSumOfAllValues() {
        if (nodesCount == 0) {
            return 0;
        }
        return getSumOfThisAndSubNodes(root);
    }
    // private helper recursive method to implement the above method

    private int getSumOfThisAndSubNodes(NodeKV node) {
        if (node == null) {
            return 0;
        }
        int result = node.value;
        result += getSumOfThisAndSubNodes(node.left);
        result += getSumOfThisAndSubNodes(node.right);
        return result;
    }

    // uses recursion to collect all values from largest to smallest
    String[] getKeysInOrder() { // from largest to smallest
        if (nodesCount == 0) {
            return new String[0];
        }
        resultKeysArray = new String[nodesCount];
        resultValArray = new int[nodesCount];
        resultIndex = 0;
        collectKeysValuesInOrder(root);
        return resultKeysArray;
    }
    // private helper recursive method to implement the above method

    private void collectKeysValuesInOrder(NodeKV node) {
        if (node == null) {
            return;
        }
        resultKeysArray[resultIndex] = node.key;
        resultValArray[resultIndex++] = node.value;
        collectKeysValuesInOrder(node.right);
        collectKeysValuesInOrder(node.left);
    }
    // data structures used to make collecting values in order easier
    private String[] resultKeysArray;
    private int[] resultValArray;
    private int resultIndex;

    void printAllKeyValPairs() {
        for (int i = 0; i < resultIndex; i++) {
            System.out.printf("Key: %s, Value: %d\n", resultKeysArray[i], resultValArray[i]);
        }
    }

    public Iterator<BinaryTreeStringInt.NodeKV> iterator() {
        return new SimpleBinaryTreeIterator();
    }

    private class SimpleBinaryTreeIterator implements Iterator<BinaryTreeStringInt.NodeKV> {

        int i;
        String[] keys;
        int[] values;

        public SimpleBinaryTreeIterator() {
            i = 0;
            keys = resultKeysArray;
            values = resultValArray;
        }

        public boolean hasNext() {
            return keys.length > i;
        }

        public BinaryTreeStringInt.NodeKV next() {
            BinaryTreeStringInt.NodeKV result = new BinaryTreeStringInt.NodeKV();
            result.key = String.valueOf(keys[i]);
            result.value = Integer.valueOf(values[i++]); 
            return result;
        }
    }
}
