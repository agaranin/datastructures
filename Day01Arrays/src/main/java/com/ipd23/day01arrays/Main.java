/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ipd23.day01arrays;

import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Random;
import java.util.Scanner;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 *
 * @author Artem
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Random r = new Random();
        System.out.println("Enter the array size:");
        int arraySize = getInt();
        int[] array = new int[arraySize];
        List<Integer> primeList = new ArrayList<>();
        for (int i = 0; i < arraySize; i++) {
            array[i] = r.nextInt(100) + 1;
            if(isPrime(array[i])){
                primeList.add(array[i]);
            }
        }
        String result = IntStream.of(array)
                .mapToObj(Integer::toString)
                .collect(Collectors.joining(", "));
        System.out.println("All numbers from the array:");
        System.out.println(result);
        String primeResult = primeList.stream()
        .map(number -> String.valueOf(number))
        .collect(Collectors.joining(", "));
        System.out.println("All prime numbers from the array:");
        System.out.println(primeResult);

    }

    public static boolean isPrime(int number) {
        int sqrt = (int) Math.sqrt(number) + 1;
        for (int i = 2; i < sqrt; i++) {
            if (number % i == 0) { 
                return false;
            }
        }
        return true;
    }

    public static int getInt() {
        Scanner input = new Scanner(System.in);
        int value = -1;
        while (value < 0) {
            try {
                if (input.hasNextInt()) {
                    value = input.nextInt();
                    if (value < 0) {
                        throw new InputMismatchException("The number is negative: " + value);
                    }
                } // end if
                else {
                    String junk = input.next();
                    throw new InputMismatchException("Input is non-digit number: " + junk);
                } // end else
            } // end try
            catch (InputMismatchException e) {
                System.out.println(e.getMessage());
                System.out.println("Enter the positive number: ");
            } // catch
        } // end while
        return value;
    } //end method getInt

}
