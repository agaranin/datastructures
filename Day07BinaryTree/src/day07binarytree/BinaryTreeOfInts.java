/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package day07binarytree;

import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.NoSuchElementException;

/**
 *
 * @author Artem
 */
public class BinaryTreeOfInts implements Iterable<Integer> {

    private class NodeOfInt {

        public NodeOfInt(int value, NodeOfInt left, NodeOfInt right) {
            this.value = value;
            this.left = left;
            this.right = right;
        }

        int value; // could also be key,value pair
        NodeOfInt left, right;
    }

    NodeOfInt root;
    private int nodesCount;

    // throws exception if put attempts to insert value that already exists (a duplicate)
    void put(int value) throws IllegalArgumentException {
        NodeOfInt newNode = new NodeOfInt(value, null, null);
        if (root == null) {
            root = newNode;
            nodesCount++;
            return;
        }
        NodeOfInt current = root;
        while (true) {
            if (current.value == value) {
                throw new IllegalArgumentException("Duplicatea are not allowed");
            }
            if (value < current.value) {
                if (current.left == null) {
                    current.left = newNode;
                    nodesCount++;
                    return;
                } else {
                    current = current.left;
                    continue;
                }
            }
            if (value > current.value) {
                if (current.right == null) {
                    current.right = newNode;
                    nodesCount++;
                    return;
                } else {
                    current = current.right;
                    continue;
                }
            }
        }

    }

    // uses recursion to compute the sum of all values in the entire tree
    public int getSumOfAllValues() {
        if (nodesCount == 0) {
            return 0;
        }
        return getSumOfThisAndSubNodes(root);
    }
    // private helper recursive method to implement the above method

    private int getSumOfThisAndSubNodes(NodeOfInt node) {
        int sum = 0;
        if (node.left == null && node.right == null) {
            sum += node.value;
            return sum;
        } else {
            sum += node.value;
            if (node.left != null) {
                sum += getSumOfThisAndSubNodes(node.left);
            }
            if (node.right != null) {
                sum += getSumOfThisAndSubNodes(node.right);
            }
        }
        return sum;
    }

    // uses recursion to collect all values from largest to smallest
    int[] getValuesInOrder() { // from largest to smallest
        if (nodesCount == 0) {
            return new int[0];
        }
        resultArray = new int[nodesCount];
        resultIndex = 0;
        collectValuesInOrder(root);
        return resultArray;
    }
    // private helper recursive method to implement the above method

    private void collectValuesInOrder(NodeOfInt node) {
        if (node.left != null || node.right != null) {
            if (node.right != null) {
                collectValuesInOrder(node.right);
            }
            resultArray[resultIndex] = node.value;
            resultIndex++;
            if (node.left != null) {
                collectValuesInOrder(node.left);
            }
        } else {
            resultArray[resultIndex] = node.value;
            resultIndex++;
            return;
        }
    }
    // data structures used to make collecting values in order easier
    private int[] resultArray;
    private int resultIndex;

    public Iterator<Integer> iterator() {
        return new SimpleBinaryTreeIterator();
    }

    private class SimpleBinaryTreeIterator implements Iterator<Integer> {

        int i;
        int[] values;

        public SimpleBinaryTreeIterator() {
            i = 0;
            values = getValuesInOrder();
        }

        public boolean hasNext() {
            return values.length > i;
        }

        public Integer next() {
            return Integer.valueOf(values[i++]);
        }
    }
}
