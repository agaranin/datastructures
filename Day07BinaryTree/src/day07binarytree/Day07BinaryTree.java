/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package day07binarytree;

/**
 *
 * @author Artem
 */
public class Day07BinaryTree {

    static void printArray(int[] data) {
        for (int i = 0; i < data.length; i++) {
            System.out.printf("%s%d", i == 0 ? "" : ", ", data[i]);
        }
    }

    public static void main(String[] args) {
        BinaryTreeOfInts bt = new BinaryTreeOfInts();
        bt.put(5);
        bt.put(3);
        bt.put(1);
        bt.put(4);
        bt.put(8);
        System.out.println(bt.getSumOfAllValues());
        printArray(bt.getValuesInOrder());
        System.out.println("");
        
        for (int n : bt) // foreach
        {
            System.out.println("value: " + n);
        }
    }

}
