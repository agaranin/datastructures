/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package day07binarytree;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Artem
 */
public class BinaryTreeOfIntsTest {

    public BinaryTreeOfIntsTest() {
    }

    /**
     * Test of put method, of class BinaryTreeOfInts.
     */
    @Test(timeout=1000)
    public void testPut() {
        BinaryTreeOfInts instance = new BinaryTreeOfInts();
        instance.put(5);
        instance.put(3);
        instance.put(1);
        instance.put(4);
        instance.put(8);
        int[] expected = {8, 5, 4, 3, 1};
        int[] actual = instance.getValuesInOrder();
        assertArrayEquals(expected, actual);
    }
@Test(expected = IllegalArgumentException.class)
    public void testPutThrowsException() throws Exception {
         BinaryTreeOfInts instance = new BinaryTreeOfInts();
        instance.put(5);
        assertEquals(5, instance.getSumOfAllValues());
        instance.put(3);
        instance.put(1);
        instance.put(4);
        instance.put(8);
        instance.put(8);
    }
    /**
     * Test of getSumOfAllValues method, of class BinaryTreeOfInts.
     */
    @Test
    public void testGetSumOfAllValues() {
        BinaryTreeOfInts instance = new BinaryTreeOfInts();
        assertEquals(0, instance.getSumOfAllValues());
        instance.put(5);
        instance.put(3);
        instance.put(1);
        instance.put(4);
        instance.put(8);
        assertEquals(21, instance.getSumOfAllValues());
    }

    /**
     * Test of getValuesInOrder method, of class BinaryTreeOfInts.
     */
    @Test
    public void testGetValuesInOrder() {
        BinaryTreeOfInts instance = new BinaryTreeOfInts();
        assertArrayEquals(new int[] {}, instance.getValuesInOrder());
        instance.put(5);
        instance.put(3);
        assertArrayEquals(new int[] {5, 3}, instance.getValuesInOrder());
        instance.put(1);
        instance.put(4);
        assertArrayEquals(new int[] {5, 4, 3, 1}, instance.getValuesInOrder());
        instance.put(8);
        int[] expected = {8, 5, 4, 3, 1};
        int[] actual = instance.getValuesInOrder();
        assertArrayEquals(expected, actual);
    }

}
