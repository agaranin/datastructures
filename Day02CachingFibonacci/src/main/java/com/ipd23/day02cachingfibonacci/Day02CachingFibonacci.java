/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ipd23.day02cachingfibonacci;

import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.Scanner;

public class Day02CachingFibonacci {

    static Scanner input = new Scanner(System.in);
    static FibCached f = new FibCached();

    static int getInt() {
        int value = -1;
        while (true) {
            try {
                value = input.nextInt();
                return value;
            } // end try
            catch (InputMismatchException e) {
                input.next(); // consume the wrong value
                System.out.println("Enter an integer: ");
            } // catch
        } // end while
    } //end method getInt

    static void GetFibByIndex() {
        while(true){
        System.out.println("Enter the number to find nth Fibonacci number:");
        try {
            int nth = getInt();
            System.out.printf("%dth Fibonacci number is: %d\n", nth, f.getNthFib(nth));
            break;

        } catch (IllegalArgumentException ex) {
            System.out.println(ex.getMessage());
        }
        }
    }

    public static void main(String[] args) {
        while (true) {
            GetFibByIndex();
            System.out.println("Steps: " + f.getCountOfFibsComputed());
            System.out.println("Cash: "+f.toString());
        }
    }
}
