/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ipd23.day02cachingfibonacci;

import java.util.ArrayList;

/**
 *
 * @author Artem
 */
class FibCached {

    private ArrayList<Long> fibsCached = new ArrayList<>();
    private int fibsCompCount = 2;

    FibCached() {
        fibsCached.add(0L); // #0
        fibsCached.add(1L); // #1
    }

    public long getNthFib(int n) {
        n--;
        if (n < 0) {
            throw new IllegalArgumentException("Index cannot be less than 1");
        }
        return computeNthFib(n);
    }

    private long computeNthFib(int n) {
        if (n >= fibsCached.size()) {
            fibsCached.add(n, computeNthFib(n - 1) + computeNthFib(n - 2));
            fibsCompCount++;
        }
        return fibsCached.get(n);
    }

  
    public int getCountOfFibsComputed() {
        return fibsCompCount;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        for (long l : fibsCached) {

            sb.append(l == 0 ? "" : ", ");
            sb.append(l);
        }
        return sb.toString();
    }
}

