/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ipd23.day03customhashmap;

import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Rule;
import org.junit.rules.Timeout;

/**
 *
 * @author Artem
 */
public class CustomHashMapStringStringTest {
    
    public CustomHashMapStringStringTest() {
    }
// maximum time given for any single test to execute
    @Rule
    public Timeout globalTimeout= new Timeout(10*1000); // 10s max for any single test
    
     @Test
    public void PutGet_1() {
        CustomHashMapStringString map = new CustomHashMapStringString();
        map.putValue("Jerry", "Blue");
        map.putValue("Jerry123", "Blue");
        map.putValue("JerryAAA", "Blue");
        map.putValue("JerryBBB", "Blue");
        map.putValue("Jerry2", "Blue");
        map.putValue("JerryNN", "Blue");
        map.putValue("Jerry", "White");
        map.putValue("Jerry", "Yellow");
        map.putValue("Jerry", "Red");
        assertEquals("Blue", map.getValue("JerryNN"));
        assertEquals("Red", map.getValue("Jerry"));
    }
    

    @Test
    public void CustomHashMapPutTest() {
        CustomHashMapStringString instance = new CustomHashMapStringString();
        instance.putValue("Jerry", "Blue");
        instance.putValue("Barry", "Violet");
        instance.putValue("Jerry", "Yellow");
        instance.putValue("Eva", "Green");
        assertEquals(3,instance.getSize());
        assertEquals("[ Barry => Violet, Eva => Green, Jerry => Yellow ]", instance.toString());
    }
    
    @Test
    public void CustomHashMapDeleteTest() {
        CustomHashMapStringString instance = new CustomHashMapStringString();
        instance.putValue("Jerry", "Blue");
        instance.putValue("Barry", "Violet");
        instance.putValue("Terry", "Yellow");
        instance.putValue("Eva", "Green");
        assertEquals(4,instance.getSize());
        assertEquals("[ Barry => Violet, Eva => Green, Jerry => Blue, Terry => Yellow ]", instance.toString());        
        // boolean result1 = instance.deleteByKey("Jimmy"); exception - create a separate method to test it
        assertEquals(4,instance.getSize());
        instance.deleteByKey("Jerry"); // no exception
        assertEquals(3,instance.getSize());
        assertEquals("[ Barry => Violet, Eva => Green, Terry => Yellow ]", instance.toString());        
    }
    
     /**
     * Test of deleteByKey method, of class CustomHashMapStringString.
     */
    @Test(expected = KeyNotFoundException.class)
    public void testDeleteByKey() {
         CustomHashMapStringString instance = new CustomHashMapStringString();
        instance.putValue("Jerry", "Blue");
        instance.putValue("Barry", "Violet");
        instance.putValue("Terry", "Yellow");
        instance.putValue("Eva", "Green");
        instance.deleteByKey("Jimmy");
    }
    
}
