/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ipd23.day03customhashmap;

import java.security.KeyException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Artem
 */
public class Day03CustomHashMap {

    static void printArray(String[] data) {
        for (int i = 0; i < data.length; i++) {
            System.out.printf("%s%s", i == 0 ? "" : ", ", data[i]);
        }
    }

    public static void main(String[] args) {
        CustomHashMapStringString h = new CustomHashMapStringString();
        for (int i = 1; i < 20; i++) {
            h.putValue("key" + i, "value" + i);
        }
        System.out.println("Size: " + h.getSize());
        System.out.println("Delete key15");
        h.deleteByKey("key15");
        h.printDebug();
        System.out.println("Size: " + h.getSize());
        printArray(h.getAllKeys());
        System.out.println(h.hasKey("key29") ? "\nKey found" : "\nKey not found");
        System.out.println(h.toString());
    }
//
}
