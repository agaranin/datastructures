/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ipd23.day03customhashmap;

import java.security.KeyException;
import java.util.Arrays;

class KeyNotFoundException extends RuntimeException {}

public class CustomHashMapStringString {

    private class Container {

        public Container(Container next, String key, String value) {
            this.next = next;
            this.key = key;
            this.value = value;
        }

        Container next;
        String key;
        String value;
    }
    private int size = 5;
    // size must be a prime number always
    private Container[] hashTable = new Container[size];

    private int totalItems = 0;

    private int computeHashValue(String key) {
        int hash = 0;
        for (int i = 0; i < key.length(); i++) {
            hash <<= 1;  // same as: hash *= 2
            char c = key.charAt(i);
            //hash += c;
            hash = hash ^ c;
        }
        return hash;
    }

    private boolean isPrime(int number) {
        int sqrt = (int) Math.sqrt(number) + 1;
        for (int i = 2; i < sqrt; i++) {
            if (number % i == 0) {
                return false;
            }
        }
        return true;
    }

    String getValue(String key) {
        int hashValue = computeHashValue(key);
        int bucketIndex = hashValue % size;
        Container current = hashTable[bucketIndex];
        while (current != null) {
            if (current.key.equals(key)) {
                return current.value;
            }
            current = current.next;
        }
        throw new KeyNotFoundException();
    }

    void putValue(String key, String value) {
        if (totalItems > size * 3) {
            expandHashTable();
        }
        int hashValue = computeHashValue(key);
        int bucketIndex = hashValue % size;

        // if the key exists, update value
        Container current = hashTable[bucketIndex];
        while (current != null) {
            if (current.key.equals(key)) {
                current.value = value;
                return;
            }
            current = current.next;
        }
        //if the key was not found
        hashTable[bucketIndex] = new Container(hashTable[bucketIndex], key, value);
        totalItems++;
    }

    private void expandHashTable() {
        size = size * 2;
        while (!isPrime(size)) {
            size++;
        }
        totalItems = 0;
        var tempHashTable = new Container[size];
        for (int i = 0; i < hashTable.length; i++) {
            tempHashTable[i] = hashTable[i];
        }
        hashTable = new Container[size];

        for (int i = 0; i < size; i++) {
            if (tempHashTable[i] != null) {
                Container current = tempHashTable[i];
                while (current != null) {
                    putValue(current.key, current.value);
                    current = current.next;
                }
            }
        }
    }

    boolean hasKey(String key) {
        int hashValue = computeHashValue(key);
        int bucketIndex = hashValue % size;
        Container current = hashTable[bucketIndex];
        while (current != null) {
            if (current.key.equals(key)) {
                return true;
            }
            current = current.next;
        }
        return false;
    }

    void deleteByKey(String key) {
        int hashValue = computeHashValue(key);
        int bucketIndex = hashValue % size;
        Container before = null;
        Container current = hashTable[bucketIndex];
        while (current != null) {
            if (current.key.equals(key)) {
                break;
            }
            before = current;
            current = current.next;
        }
        if (current == null) {
            throw new KeyNotFoundException();
        }
        if (before == null) { // removing the first item
            hashTable[bucketIndex] = current.next;
        } else {
            before.next = current.next;
        }
        totalItems--;
    }

    public String[] getAllKeys() {
        var result = new String[totalItems];
        int count = 0;
        for (int i = 0; i < hashTable.length; i++) {
            if (hashTable[i] != null) {
                Container current = hashTable[i];
                while (current != null) {
                    result[count] = current.key;
                    count++;
                    current = current.next;
                }
            }
        }
        Arrays.sort(result);
        return result;
    }

    int getSize() {
        return totalItems;
    }

    public void printDebug() {

        for (int i = 0; i < size; i++) {
            System.out.printf("Entry %d:\n", i);
            if (hashTable[i] != null) {
                Container current = hashTable[i];
                while (current != null) {
                    System.out.printf("- Key %s, Value: %s\n", current.key, current.value);
                    current = current.next;
                }
            }
        }
    } // print hashTable content, see example below

    @Override
    public String toString() {
        var keys = getAllKeys();
        StringBuilder sb = new StringBuilder();
        sb.append("[ ");
        for (int i = 0; i < keys.length; i++) {
            sb.append(i == 0 ? "" : ", ");
            sb.append(keys[i]);
            sb.append(" => ");
            sb.append(getValue(keys[i]));
        }
        sb.append(" ]");
        return sb.toString();
    } // comma-separated values->key pair list
    // to be able to use this as validation in Unit tests keys must be sorted
    // e.g. [ Key1 => Val1, Key2 => Val2, ... ]
}
