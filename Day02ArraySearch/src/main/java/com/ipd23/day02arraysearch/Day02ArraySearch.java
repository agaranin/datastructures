/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ipd23.day02arraysearch;

/**
 *
 * @author Artem
 */
public class Day02ArraySearch {
    static void print2D(int[][] data2d) {
        for (int row = 0; row < data2d.length; row++) {
            for (int col = 0; col < data2d[row].length; col++) {
                System.out.printf("%s%d", col == 0 ? "" : ", ", data2d[row][col]);
            }
            System.out.println("");
        }
        System.out.println("");
    }

    static int getIfExists(int[][] data, int row, int col) {
            if(row <0 || col<0) return 0;
            if(data.length > row && data[row].length > col){
	     return data[row][col];
            } else{
            return 0;
            }
    }
    
    static int sumOfCross(int[][] data, int row, int col) {
	    int n1 = getIfExists(data, row, col);
            int n2 = getIfExists(data, row, col-1);
            int n3 = getIfExists(data, row, col+1);
            int n4 = getIfExists(data, row-1, col);
            int n5 = getIfExists(data, row+1, col);
            int sum = n1+n2+n3+n4+n5;
            return sum;
    }
    
    static void searchSmallestSum(int[][] data){
        int min = sumOfCross(data, 0, 0);
        int rowFound = 0;
        int colFound = 0;
        for (int row = 0; row < data.length; row++) {
            for (int col = 0; col < data[row].length; col++) {
                if(min > sumOfCross(data, row, col)){
                    min = sumOfCross(data, row, col);
                    rowFound = row;
                    colFound = col;
                }
            }
        }
        System.out.printf("Element at row %d and col %d has the smallest sum", rowFound, colFound);
    }
    static int[][] getData2Dsums(int[][] data){
        int[][] Data2Dsums = new int[data.length][];
        
        for (int row = 0; row < data.length; row++) {
            Data2Dsums[row] = new int[data[row].length];
            for (int col = 0; col < data[row].length; col++) {
                Data2Dsums[row][col] = sumOfCross(data, row, col);
            }
        }
        return Data2Dsums;
    }
    
    public static void main(String[] args) {
        int[][] data2d = {
            new int[]{2, 7, 0, 11},
            new int[]{9, 4},
            new int[]{5, 6, 7, 8, 9, 4},
            new int[]{6, 7, 8, 9, 2}
        };
        print2D(data2d);
        System.out.println(getIfExists(data2d, 1, 0));
        System.out.println(sumOfCross(data2d, 1, 2));
        searchSmallestSum(data2d);
        System.out.println("");
        print2D(getData2Dsums(data2d));
        
    }
    
}
