/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ipd23.day03arraylistownimpl;

import org.junit.Assert;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author Artem
 */
public class CustomArrayOfIntsTest {
    
    public CustomArrayOfIntsTest() {
    }

    /**
     * Test of size method, of class CustomArrayOfInts.
     */
    @Test
    public void testSize() {
        CustomArrayOfInts a = new CustomArrayOfInts();
        a.add(1);
        a.add(4);
        int expected = 2;
        int actual = a.size();
        assertEquals(expected, actual);
    }
     /**
     * Test of size method, of class CustomArrayOfInts.
     */
    @Test
    public void testZeroSize() {
        CustomArrayOfInts a = new CustomArrayOfInts();
        int expected = 0;
        int actual = a.size();
        assertEquals(expected, actual);
    }

    /**
     * Test of add method, of class CustomArrayOfInts.
     */
    @Test
    public void testAdd() {
        CustomArrayOfInts a = new CustomArrayOfInts();
        int expected = 23;
        a.add(expected);
        int actual = a.get(0);
        assertEquals(expected, actual);
    }

    /**
     * Test of deleteByIndex method, of class CustomArrayOfInts.
     */
    @Test
    public void testDeleteByIndex() {
        CustomArrayOfInts a = new CustomArrayOfInts();
        a.add(23);
        a.add(24);
        a.deleteByIndex(0);
        int expected = 24;
        int actual = a.get(0);
        assertEquals(expected, actual);
    }
    /**
     * Test of deleteByIndex method, of class CustomArrayOfInts.
     */
   @Test
    public void testDeleteByIndexThrowsIndexOutOfBoundsException() {
        CustomArrayOfInts a = new CustomArrayOfInts();
        Throwable exception = assertThrows(IndexOutOfBoundsException.class, () -> a.deleteByIndex(1));
    }
    /**
     * Test of deleteByIndex method, of class CustomArrayOfInts.
     */
   @Test
    public void testDeleteByIndexThrowsIndexOutOfBoundsExceptionWhenIndexIsNegative() {
        CustomArrayOfInts a = new CustomArrayOfInts();
        Throwable exception = assertThrows(IndexOutOfBoundsException.class, () -> a.deleteByIndex(-2));
    }
    

    /**
     * Test of deleteByValue method, of class CustomArrayOfInts.
     */
    @Test
    public void testDeleteByValue() {
        CustomArrayOfInts a = new CustomArrayOfInts();
        a.add(23);
        a.add(24);
        a.deleteByValue(23);
        int actual = a.get(0);
        int expected = 24;
        assertEquals(expected, actual);
    }

    /**
     * Test of insertValueAtIndex method, of class CustomArrayOfInts.
     */
    @Test
    public void testInsertValueAtIndex() {
        CustomArrayOfInts a = new CustomArrayOfInts();
        a.add(23);
        a.add(24);
        a.add(10);
        a.insertValueAtIndex(15, 2);
        int actual = a.get(2);
        int expected = 15;
        assertEquals(expected, actual);
    }
    /**
     * Test of insertValueAtIndex method, of class CustomArrayOfInts.
     */
    @Test
    public void testInsertValueAtIndexThrowsIndexOutOfBoundsExceptionWhenIndexIsNegative() {
         CustomArrayOfInts a = new CustomArrayOfInts();
        Throwable exception = assertThrows(IndexOutOfBoundsException.class, () -> a.insertValueAtIndex(2,-2));
    }
    /**
     * Test of insertValueAtIndex method, of class CustomArrayOfInts.
     */
    @Test
    public void testInsertValueAtIndexThrowsIndexOutOfBoundsException() {
         CustomArrayOfInts a = new CustomArrayOfInts();
         assertThrows(IndexOutOfBoundsException.class, () -> a.insertValueAtIndex(2,1));
    }

    /**
     * Test of clear method, of class CustomArrayOfInts.
     */
    @Test
    public void testClear() {
        CustomArrayOfInts a = new CustomArrayOfInts();
        a.add(23);
        a.add(24);
        a.add(10);
        a.clear();
        int actual = a.size();
        int expected = 0;
        assertEquals(expected, actual);
    }

    /**
     * Test of get method, of class CustomArrayOfInts.
     */
    @Test
    public void testGet() {
        CustomArrayOfInts a = new CustomArrayOfInts();
        a.add(23);
        int expected = 23;
        int actual = a.get(0);
        assertEquals(expected, actual);
    }

    /**
     * Test of getSlice method, of class CustomArrayOfInts.
     */
    @Test
    public void testGetSlice() {
        CustomArrayOfInts a = new CustomArrayOfInts();
        a.add(23);
        a.add(13);
        a.add(33);
        a.add(43);
        int[] expected = {13, 33, 43};
        int[] actual = a.getSlice(1, 3);
        Assert.assertArrayEquals(expected, actual);
    }

    /**
     * Test of toString method, of class CustomArrayOfInts.
     */
    @Test
    public void testToString() {
         CustomArrayOfInts a = new CustomArrayOfInts();
        a.add(23);
        a.add(13);
        a.add(33);
        a.add(43);
        String expected = "[23, 13, 33, 43]";
        String actual = a.toString();
        Assert.assertEquals(expected, actual);
    }
    
}
