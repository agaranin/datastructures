/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ipd23.day03arraylistownimpl;

/**
 *
 * @author Artem
 */
public class Day03ArrayListOwnImpl {

    static CustomArrayOfInts a = new CustomArrayOfInts();

    static void printArray(int[] data) {
        for (int i = 0; i < data.length; i++) {
            System.out.printf("%s%d", i == 0 ? "" : ", ", data[i]);
        }
    }

    public static void main(String[] args) {
        System.out.println("Add 5 numbers:");
        a.add(6);
        a.add(7);
        a.add(5);
        a.add(63);
        a.add(66);
        System.out.println("Size of the array: " + a.size());
        System.out.println(a.toString());
//        System.out.println("Delete by index 3:");
//        a.deleteByIndex(3);
//        System.out.println(a.toString());
//        System.out.println("Size of the array: " + a.size());
//        
//        System.out.println("Delete value 5:");
//        a.deleteByValue(5);
//        System.out.println(a.toString());
//        System.out.println("Size of the array: " + a.size());
//        
//        System.out.println("Insert value 9 at index 2:");
//        a.insertValueAtIndex(9, 2);
//        System.out.println(a.toString());
//        System.out.println("Size of the array: " + a.size());
//        
//        System.out.println("Get value at index 2:");
//        System.out.println("Value at index 2: " + a.get(2));
//        
//        System.out.println("Get slice of 3 at index 1:");
//        
//        System.out.print("Slice: ");
//        printArray(a.getSlice(1, 3));
//        
//        System.out.println("\nClear array");
//        a.clear();
//        System.out.println(a.toString());
//        System.out.println("Size of the array: " + a.size());
    }

}
