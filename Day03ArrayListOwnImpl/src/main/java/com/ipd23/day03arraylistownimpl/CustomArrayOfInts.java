/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ipd23.day03arraylistownimpl;

/**
 *
 * @author Artem
 */
public class CustomArrayOfInts {

    private int[] data = new int[1]; // only grows by doubling size, never shrinks
    private int size = 0; // how many items do you really have

    public int size() {
        return size;
    }
    private void growStorage() {
        int[] newData = new int[data.length*2]; // double the size (heuristic)
        // copy the old array content into the new one
        for (int i = 0; i < data.length; i++) {
            newData[i] = data[i];
        }
        data = newData;
    }
    private void shrinkStorage() {
        int[] newData = new int[data.length/2]; // double the size (heuristic)
        // copy the old array content into the new one
        for (int i = 0; i < newData.length; i++) {
            newData[i] = data[i];
        }
        data = newData;
    }

    public void add(int value) {
        if(data.length == size){
           growStorage();
        }
        data[size++] = value;
    }

    public void deleteByIndex(int index) {
        if(index >= size || index < 0){
            throw new IndexOutOfBoundsException();
        }
        for (int i = index + 1; i < size; i++) {
            data[i-1] = data[i];
        }
         size--;
         if(size <=(data.length/4)){
            shrinkStorage();
        }
    }

    public boolean deleteByValue(int value) {
        for (int i = 0; i < size; i++) {
            if(data[i] == value){
                deleteByIndex(i);
                return true;
            }
        }
        return false;
    } // delete first value matching, true if value found, false otherwise

    public void insertValueAtIndex(int value, int index) {
        if(index > size || index < 0){
            throw new IndexOutOfBoundsException();
        }
        if(data.length == size){
            growStorage();
        }
        for (int i = size - 1; i >= index; i--) {
            data[i + 1] = data[i];
        }
        data[index] = value;
        size++;
    }

    public void clear() {
        size = 0;
    }

    public int get(int index) {
        if(index >= size || index < 0){
            throw new IndexOutOfBoundsException();
        }
        return data[index];
    } // may throw IndexOutOfBoundsException

    public int[] getSlice(int startIdx, int length) {
        if(startIdx+length > size || startIdx < 0 || length < 0){
            throw new IndexOutOfBoundsException();
        }
        var slice = new int[length];
        for (int i = 0; i < length; i++) {
            slice[i] = data[i + startIdx];
        }
        return slice;
    } // may throw IndexOutOfBoundsException

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("[");
            for (int i = 0; i < size; i++) {
            sb.append(i == 0 ? "" : ", ");
            sb.append(data[i]);
        }
        sb.append("]");
        return sb.toString();
    } // returns String similar to: [3, 5, 6, -23]
}
