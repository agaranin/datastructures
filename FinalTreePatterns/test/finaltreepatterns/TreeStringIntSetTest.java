/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package finaltreepatterns;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Artem
 */
public class TreeStringIntSetTest {
     @Test(timeout = 1000)
    public void testAdd() throws DuplicateValueException {

        TreeStringIntSet tree = new TreeStringIntSet();
        assertEquals(tree.getValuesByKey("aaa").toString(),"[]" );
        tree.add("aaa", 2);
        assertEquals(tree.getValuesByKey("aaa").toString(),"[2]" );
        tree.add("aaa", 3);
        assertEquals(tree.getValuesByKey("aaa").toString(),"[2, 3]" );
        tree.add("ddd", 2);
        tree.add("ccc", 2);
        tree.add("fff", 6);
        tree.add("eee", 2);
        tree.add("fff", 3);
        tree.add("fff", 5);
        assertEquals(tree.getValuesByKey("fff").toString(),"[3, 5, 6]" );
    }

    @Test(timeout = 1000)
    public void testIterableInterface() throws DuplicateValueException {

        TreeStringIntSet tree = new TreeStringIntSet();
        tree.add("aaa", 2);
        tree.add("bbb", 2);
        tree.add("ddd", 2);
        tree.add("ccc", 2);
        tree.add("fff", 6);
        tree.add("eee", 2);
        tree.add("fff", 3);
        tree.add("fff", 5);
  
        String[] arrayKeysExpected = new String[]{"aaa", "bbb", "ccc", "ddd", "eee", "fff", "fff", "fff"};
        String[] arrayKeysActual = new String[8];
        int[] arrayValuesExpected = new int[]{2, 2, 2, 2, 2, 3, 5, 6};
        int[] arrayValuesActual = new int[8];
        int i = 0;
        for (Pair<String, Integer> pair : tree) {
            arrayKeysActual[i] = pair.key;
            arrayValuesActual[i++] = pair.value;
        }
        assertArrayEquals(arrayKeysExpected, arrayKeysActual);
        assertArrayEquals(arrayValuesExpected, arrayValuesActual);
    }

    @Test(expected = DuplicateValueException.class)
    public void testAddThrowsException() throws DuplicateValueException {

        TreeStringIntSet tree = new TreeStringIntSet();
        tree.add("aaa", 2);
        tree.add("bbb", 2);
        tree.add("ddd", 2);
        tree.add("ccc", 2);
        tree.add("fff", 6);
        tree.add("eee", 2);
        tree.add("fff", 3);
        tree.add("fff", 5);
        tree.add("fff", 5);
    }

    /**
     * Test of containsKey method, of class TreeStringIntSet.
     */
    @Test(timeout = 1000)
    public void testContainsKey() throws DuplicateValueException {
        TreeStringIntSet tree = new TreeStringIntSet();
        tree.add("aaa", 2);
        tree.add("bbb", 2);
        tree.add("ddd", 2);
        tree.add("ccc", 2);
        tree.add("fff", 6);
        tree.add("eee", 2);
        tree.add("fff", 3);
        tree.add("fff", 5);
        assertEquals(true, tree.containsKey("bbb"));
        assertEquals(false, tree.containsKey("asd"));
    }

    /**
     * Test of getValuesByKey method, of class TreeStringIntSet.
     */
    @Test(timeout = 1000)
    public void testGetValuesByKey() throws DuplicateValueException {
        TreeStringIntSet tree = new TreeStringIntSet();
        tree.add("aaa", 2);
        tree.add("bbb", 2);
        tree.add("ddd", 2);
        tree.add("ccc", 2);
        tree.add("fff", 6);
        tree.add("eee", 2);
        tree.add("fff", 3);
        tree.add("fff", 5);
        String expected = "[3, 5, 6]";
        String actual = tree.getValuesByKey("fff").toString();
        assertEquals(expected, actual);
    }

    /**
     * Test of getKeysContainingValue method, of class TreeStringIntSet.
     */
    @Test(timeout = 1000)
    public void testGetKeysContainingValue() throws DuplicateValueException {
        TreeStringIntSet tree = new TreeStringIntSet();
        tree.add("aaa", 2);
        tree.add("bbb", 2);
        tree.add("ddd", 2);
        tree.add("ccc", 2);
        tree.add("fff", 6);
        tree.add("eee", 2);
        tree.add("fff", 3);
        tree.add("fff", 5);
        String expected = "[aaa, bbb, ccc, ddd, eee]";
        String actual = tree.getKeysContainingValue(2).toString();
        assertEquals(expected, actual);
    }

    /**
     * Test of getAllKeys method, of class TreeStringIntSet.
     */
    @Test(timeout = 1000)
    public void testGetAllKeys() throws DuplicateValueException {
        TreeStringIntSet tree = new TreeStringIntSet();
        tree.add("aaa", 2);
        tree.add("bbb", 2);
        tree.add("ddd", 2);
        tree.add("ccc", 2);
        tree.add("fff", 6);
        tree.add("eee", 2);
        tree.add("fff", 3);
        tree.add("fff", 5);
        String expected = "[aaa, bbb, ccc, ddd, eee, fff]";
        String actual = tree.getAllKeys().toString();
        assertEquals(expected, actual);
    }

}
