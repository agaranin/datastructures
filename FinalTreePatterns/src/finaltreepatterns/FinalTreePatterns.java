/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package finaltreepatterns;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Artem
 */
public class FinalTreePatterns {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        TreeStringIntSet tree = new TreeStringIntSet();
        try {
            tree.add("aaa", 2);
            tree.add("bbb", 2);
            tree.add("ddd", 2);
            tree.add("ccc", 2);
            tree.add("fff", 6);
            tree.add("eee", 2);
            tree.add("fff", 3);
            tree.add("fff", 5);
            
            System.out.println(tree.containsKey("bbb")?"Tree contains key": "Key not found");
            System.out.println(tree.getValuesByKey("fff").toString());
            System.out.println(tree.getKeysContainingValue(2).toString());
            System.out.println(tree.getAllKeys().toString());
            
            for (Pair<String,Integer> pair : tree) {
                System.out.println(pair.toString());
}
            
        } catch (DuplicateValueException ex) {
            System.out.println("Duplicate value");
        }
    }
    
}
