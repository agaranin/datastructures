/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package finaltreepatterns;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;

/**
 *
 * @author Artem
 */
class DuplicateValueException extends Exception {
}

public class TreeStringIntSet implements Iterable<Pair<String, Integer>> {

    @Override
    public Iterator<Pair<String, Integer>> iterator() {
        return new FinalTreeIterator();
    }

    private class FinalTreeIterator implements Iterator<Pair<String, Integer>> {

        int resultIndex;
        int i;
        Pair<String, Integer>[] arrayOfPairs;

        public FinalTreeIterator() {
            i = 0;
            resultIndex = 0;
            arrayOfPairs = new Pair[valuesCount];
            collectPairs(root);
        }

        private void collectPairs(Node node) {
            if (node == null) {
                return;
            }
            collectPairs(node.left);
            for (Integer value : node.valuesSet) {
                arrayOfPairs[resultIndex++] = new Pair<String, Integer>(node.key, value);
            }
            collectPairs(node.right);

        }

        public boolean hasNext() {
            return arrayOfPairs.length > i;
        }

        public Pair<String, Integer> next() {
            Pair<String, Integer> result = new Pair();
            result = arrayOfPairs[i++];
            return result;
        }
    }

    class Node {

        Node left, right;
        String key; // keys are unique
        // HashSet is like ArrayList except it does not hold duplicates
        HashSet<Integer> valuesSet = new HashSet<>(); // unique only
    }
    private Node root;
    int nodesCount;
    int valuesCount;

    // throws DuplicateValueException if this key already contains such value
    void add(String key, int value) throws DuplicateValueException {
        Node newNode = new Node();
        newNode.key = key;
        newNode.valuesSet.add(value);
        if (root == null) {
            root = newNode;
            nodesCount++;
            valuesCount++;
            return;
        }
        Node current = root;
        while (true) {
            if (current.key.equals(key)) {
                if (current.valuesSet.contains(value)) {
                    throw new DuplicateValueException();
                } else {
                    current.valuesSet.add(value);
                    nodesCount++;
                    valuesCount++;
                    return;
                }
            }
            if (key.compareTo(current.key) < 0) {
                if (current.left == null) {
                    current.left = newNode;
                    nodesCount++;
                    valuesCount++;
                    return;
                } else {
                    current = current.left;
                    continue;
                }
            }
            if (key.compareTo(current.key) > 0) {
                if (current.right == null) {
                    current.right = newNode;
                    nodesCount++;
                    valuesCount++;
                    return;
                } else {
                    current = current.right;
                }
            }
        }
    }

    boolean containsKey(String key) {
        Node current = root;
        while (true) {
            if (current.key.equals(key)) {
                return true;
            }
            if (key.compareTo(current.key) < 0) {
                if (current.left == null) {
                    return false;
                } else {
                    current = current.left;
                    continue;
                }
            }
            if (key.compareTo(current.key) > 0) {
                if (current.right == null) {
                    return false;
                } else {
                    current = current.right;
                }
            }
        }
    }

    List<Integer> getValuesByKey(String key) {
        Node current = root;
        ArrayList<Integer> result = new ArrayList<Integer>();
        while (true) {
            if (current == null) {
                return result;
            }
            if (current.key.equals(key)) {

                for (Integer value : current.valuesSet) {
                    result.add(value);
                }
                return result;
            }
            if (key.compareTo(current.key) < 0) {
                if (current.left == null) {
                    return result;
                } else {
                    current = current.left;
                    continue;
                }
            }
            if (key.compareTo(current.key) > 0) {
                if (current.right == null) {
                    return result;
                } else {
                    current = current.right;
                }
            }
        }
    } // return empty list if key not found

    private void collectKeysContainingValue(Node node, int value) {
        if (node == null) {
            return;
        }
        collectKeysContainingValue(node.left, value);
        if (node.valuesSet.contains(value)) {
            result.add(node.key);
        }
        collectKeysContainingValue(node.right, value);
    }
    ArrayList<String> result;

    List<String> getKeysContainingValue(int value) {
        result = new ArrayList<String>();
        collectKeysContainingValue(root, value);
        return result;
    }

    private void collectAllKeys(Node node) {
        if (node == null) {
            return;
        }
        collectAllKeys(node.left);
        resultAllKeys.add(node.key);
        collectAllKeys(node.right);
    }

    ArrayList<String> resultAllKeys;

    List<String> getAllKeys() {
        resultAllKeys = new ArrayList<String>();
        collectAllKeys(root);
        return resultAllKeys;
    }
}

class Pair<K, V> {

    K key;
    V value;

    public Pair() {
    }

    Pair(K key, V value) {
        this.key = key;
        this.value = value;
    }

    @Override
    public String toString() {
        return String.format("(%s=>%s)", key.toString(), value.toString());
    }
}

interface TreeEventObserverInt {

    public void event(String key, int value, String operation);
}
